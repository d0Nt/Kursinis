package Mysql;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.sql.*;

public class Mysql {
    private MysqlDataSource dataSource;
    private Connection conn = null;
    private String lastError = "";
    private Statement lastStatement = null;
    private PreparedStatement lastPreparedStatement = null;
    private ResultSet lastResultSet = null;
    public Mysql(String user, String password, String host, String db){
        dataSource = new MysqlDataSource();
        dataSource.setUser(user);
        dataSource.setPassword(password);
        dataSource.setServerName(host);
        dataSource.setDatabaseName(db);
        dataSource.setEncoding("utf-8");
        dataSource.setZeroDateTimeBehavior("convertToNull");
    }
    public void connect(){
        if(isConnected()) return;
        try {
            conn = dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println("Nepavyko prisijungti prie mysql.\nKlaida: "+e.getMessage());
            lastError=e.getMessage();
            conn=null;
            System.exit(0);
        }
    }
    public String getLastError(){
        return lastError;
    }
    public void queryExecute(String query){
        try {
            lastStatement = conn.createStatement();
            lastStatement.execute(query);
        } catch (SQLException e) {
            lastError=e.getMessage();
        }
    }
    public ResultSet queryExecuteResult(String query){
        try {
            lastStatement = conn.createStatement();
            lastResultSet = lastStatement.executeQuery(query);
            return lastResultSet;
        } catch (SQLException e) {
            lastError=e.getMessage();
            return null;
        }
    }
    public PreparedStatement startPreparedStatement(String query){
        try {
            lastPreparedStatement = conn.prepareStatement(query);
            return lastPreparedStatement;
        } catch (SQLException e) {
            return null;
        }
    }
    public ResultSet preparedExecuteResult(PreparedStatement preparedStatement){
        try {
            lastResultSet = preparedStatement.executeQuery();
            return lastResultSet;
        } catch (SQLException e) {
            lastError=e.getMessage();
            return null;
        }
    }
    public void preparedExecute(PreparedStatement preparedStatement){
        try {
            preparedStatement.execute();
        } catch (SQLException e) {
            lastError=e.getMessage();
        }
    }
    public void closeLastQuery(){
        try {
            if(lastResultSet!=null) {
                lastResultSet.close();
                lastResultSet=null;
            }
            if(lastStatement!=null) {
                lastStatement.close();
                lastStatement=null;
            }
            if(lastPreparedStatement!=null) {
                lastPreparedStatement.close();
                lastPreparedStatement = null;
            }
        } catch (SQLException e) {
            lastError=e.getMessage();
        }
    }
    public boolean isConnected(){
        return conn != null;
    }
    public void close(){
        if(!isConnected()) return;
        try {
            conn.close();
            conn=null;
        } catch (SQLException e) {
            lastError=e.getMessage();
        }
    }
}
