package Mysql.Controllers;

import Mysql.Mysql;
import Objects.Entry;
import Objects.Hash;
import Objects.Project;

import java.sql.*;
import java.util.ArrayList;

public class Projects {
    private static ArrayList<Project> projectList = new ArrayList<>();
    private static Mysql connectionInstance=null;
    public static void setConnectionInstance(Mysql connection){
        connectionInstance=connection;
    }
    public static ArrayList<Project> getProjectList(){
        return projectList;
    }
    public static boolean loadAll(){
        ResultSet rs = connectionInstance.queryExecuteResult("SELECT * FROM Projects");
        projectList.clear();
        if(rs==null) return false;
        try {
            while(rs.next()){
                Project project = new Project(rs.getString("project_name"), rs.getInt("id"));
                project.setTypeFromInt(rs.getInt("project_type"));
                project.setDate(rs.getTimestamp("create_date"));
                projectList.add(project);
            }
            connectionInstance.closeLastQuery();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    public static void ReloadAll(){
        projectList.clear();
        loadAll();
    }
    public static Project getProjectById(int id){
        if(id<0) return null;
        for (Project project:projectList) {
            if(project.getProjectID()==id)
                return project;
        }
        return null;
    }
    public static Project getProjectByListId(int id){
        if(id<0) return null;
        return projectList.get(id);
    }
    public static void changeProjectName(int projectID, String newName){
        Project project=getProjectById(projectID);
        if(project==null) return;
        PreparedStatement st = connectionInstance.startPreparedStatement("UPDATE Projects SET project_name=? WHERE id="+project.getProjectID());
        try {
            st.setString(1, newName);
            connectionInstance.preparedExecute(st);
            connectionInstance.closeLastQuery();
        } catch (SQLException e) {
            System.out.println("Failed to execute query.");
            System.out.println(e.getMessage());
        }
    }
    public static void deleteProject(int projectID){
        Project project=getProjectById(projectID);
        if(project==null) return;
        connectionInstance.queryExecute("DELETE FROM Projects WHERE id="+project.getProjectID());
        connectionInstance.closeLastQuery();
        projectList.remove(project);
    }
    public static void addProject(Project project){
        if(project==null) return;
        PreparedStatement st = connectionInstance.startPreparedStatement("INSERT INTO Projects(project_name, project_password, project_type, create_date) VALUES(?,?,?,?)");
        try {
            st.setString(1, project.getProjectName());
            st.setString(2, Hash.sha256(project.getNewPassword()));
            st.setInt(3, project.getProjectType().ordinal());
            st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            connectionInstance.preparedExecute(st);
            connectionInstance.closeLastQuery();
            ReloadAll();
        } catch (SQLException e) {
            System.out.println("Failed to execute query.");
            System.out.println(e.getMessage());
        }
    }
    public static boolean checkPassword(int projectID, String password){
        ResultSet rs = connectionInstance.queryExecuteResult("SELECT * FROM Projects WHERE id="+projectID+" AND project_password='"+ Hash.sha256(password)+"'");
        try {
        if(rs==null)
            return false;
        else if(!rs.next()){
            rs.close();
            return false;
        }
        else{
            rs.close();
            return true;
        }
        }catch (SQLException e) {
            return false;
        }
    }

    public static void updateProjectType(int projectID) {
        Project project=getProjectById(projectID);
        if(project==null) return;
        connectionInstance.queryExecute("UPDATE Projects SET project_type="+project.getProjectType().ordinal()+" WHERE id="+project.getProjectID());
        connectionInstance.closeLastQuery();
    }
    public static void loadProjectEntries(int projectID){
        Project project=getProjectById(projectID);
        if(project==null) return;
        project.clearEntries();
        ResultSet rs = connectionInstance.queryExecuteResult("SELECT * FROM List WHERE project_id="+project.getProjectID()+" ORDER BY id DESC");
        if(rs==null) return;
        try {
            while(rs.next()){
                Entry entry = new Entry(rs.getInt("id"), rs.getInt("project_id"));
                entry.setDate(rs.getTimestamp("date"));
                entry.setTypeFromInt(rs.getInt("type"));
                entry.setAmount(rs.getInt("amount"));
                entry.setDescription(rs.getString("description"));
                project.addEntry(entry);
            }
            connectionInstance.closeLastQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveEntry(Entry entry) {
        if(entry==null) return;
        PreparedStatement st = connectionInstance.startPreparedStatement("UPDATE List SET description=?, type=?, amount=? WHERE id="+entry.getId());
        try {
            st.setString(1, entry.getDescription());
            st.setInt(2, entry.getType().ordinal());
            st.setInt(3, entry.getAmount());
            connectionInstance.preparedExecute(st);
            connectionInstance.closeLastQuery();
        } catch (SQLException e) {
            System.out.println("Failed to execute query.");
            System.out.println(e.getMessage());
        }
    }

    public static void deleteEntry(Entry entry) {
        if(entry==null) return;
        connectionInstance.queryExecute("DELETE FROM List WHERE id="+entry.getId());
        connectionInstance.closeLastQuery();
    }

    public static void addEntry(Entry entry) {
        if(entry==null) return;
        PreparedStatement st = connectionInstance.startPreparedStatement("INSERT INTO List(project_id, date, description, type, amount) VALUES(?,?,?,?,?)");
        try {
            st.setInt(1, entry.getProjectId());
            st.setTimestamp(2, (Timestamp) entry.getDate());
            st.setString(3, entry.getDescription());
            st.setInt(4, entry.getType().ordinal());
            st.setInt(5, entry.getAmount());
            connectionInstance.preparedExecute(st);
            connectionInstance.closeLastQuery();
        } catch (SQLException e) {
            System.out.println("Failed to execute query.");
            System.out.println(e.getMessage());
        }
    }
}
