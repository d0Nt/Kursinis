-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2017 m. Grd 12 d. 21:27
-- Server version: 5.5.55-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `Kursinis`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `List`
--

CREATE TABLE `List` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(256) NOT NULL,
  `type` tinyint(2) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `Projects`
--

CREATE TABLE `Projects` (
  `id` int(11) NOT NULL,
  `project_name` varchar(128) NOT NULL,
  `project_password` varchar(64) NOT NULL,
  `project_type` tinyint(2) NOT NULL DEFAULT '0',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `Projects`
--

INSERT INTO `Projects` (`id`, `project_name`, `project_password`, `project_type`, `create_date`) VALUES
(1, 'Test projektas', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 0, '0000-00-00 00:00:00'),
(2, 'Blackjack', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 0, '0000-00-00 00:00:00'),
(16, 'tedsgsd', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 0, '0000-00-00 00:00:00'),
(17, 'tedsgsd', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 1, '0000-00-00 00:00:00'),
(22, 'test', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 1, '2017-12-12 21:30:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `List`
--
ALTER TABLE `List`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `Projects`
--
ALTER TABLE `Projects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `List`
--
ALTER TABLE `List`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Projects`
--
ALTER TABLE `Projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Apribojimai eksportuotom lentelėm
--

--
-- Apribojimai lentelei `List`
--
ALTER TABLE `List`
  ADD CONSTRAINT `List_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `Projects` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;
