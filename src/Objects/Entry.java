package Objects;

import Objects.Definitions.EntryType;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

public class Entry {
    private int id=-1;
    private int project_id;
    private String description;
    private Date date;
    private EntryType type;
    private int amount;


    public Entry(int id, int project_id) {
        this.id = id;
        this.project_id = project_id;
    }
    public Entry(int project_id) {
        this.project_id = project_id;
        this.date = new Timestamp(System.currentTimeMillis());
    }
    public void setDescription(String description){
        this.description=description;
    }
    public void setDate(Timestamp date){
        this.date = date;
    }
    public void setType(EntryType type){
        this.type = type;
    }
    public void setAmount(int amount){
        this.amount=amount;
    }

    public int getId() {
        return id;
    }

    public int getProjectId() {
        return project_id;
    }

    public String getDescription() {
        return description;
    }

    public Date getDate() {
        return date;
    }

    public EntryType getType() {
        return type;
    }
    public void setTypeFromInt(int type){
        if(type<0 || type >= EntryType.values().length){
            this.type=EntryType.POSITIVE;
        }
        else this.type=EntryType.values()[type];
    }
    public EntryType getEntryTypeFromString(String type){
        for (EntryType _type:EntryType.values()) {
            if(_type.getName().equals(type)){
                return _type;
            }
        }
        return null;
    }
    public int getAmount() {
        return amount;
    }
}
