package Objects.Definitions;

public enum ProjectType {
    PERSONAL,
    BUSINESS;
    public String getName(){
        switch (this){
            case PERSONAL:
                return "Asmeninis biudžetas";
            case BUSINESS:
                return "Verslo biudžetas";
            default:
                return "Nenustatyta";
        }
    }

}
