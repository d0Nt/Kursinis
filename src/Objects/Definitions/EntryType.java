package Objects.Definitions;

public enum EntryType {
    POSITIVE,
    NEGATIVE;
    public String getName(){
        switch (this){
            case POSITIVE:
                return "Pajamos";
            case NEGATIVE:
                return "Išlaidos";
            default:
                return "Nenustatyta";
        }
    }
}
