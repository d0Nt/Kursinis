package Objects;

import Mysql.Controllers.Projects;
import Objects.Definitions.EntryType;
import Objects.Definitions.ProjectType;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Project {
    private String projectName="Unset";
    private int projectId = 0;
    private String newPassword="";
    private ProjectType projectType;
    private Date create_date = new Timestamp(System.currentTimeMillis());
    private ArrayList<Entry> projectEntries = new ArrayList<>();
    private int income = 0;
    private int outcome = 0;
    private int incomeCount=0;
    private int outcomeCount=0;
    public Project(String projectName, int projectId) {
        this.projectName = projectName;
        this.projectId = projectId;
    }

    public Project() {

    }
    public void setNewPassword(String pass){
        newPassword=pass;
    }
    public String getNewPassword(){
        return newPassword;
    }
    public void addEntry(Entry entry){
        projectEntries.add(entry);
    }
    public void removeEntry(Entry entry){
        projectEntries.remove(entry);
    }
    public void clearEntries(){
        projectEntries.clear();
    }
    public int entriesCount(){
        return projectEntries.size();
    }
    public int entriesSum(){
        income=0;
        outcome=0;
        incomeCount=0;
        outcomeCount=0;
        for (Entry e:projectEntries) {
            if(e.getType()==EntryType.POSITIVE){
                income+=e.getAmount();
                incomeCount++;
            }
            else{
                outcome+=e.getAmount();
                outcomeCount++;
            }
        }
        return income-outcome;
    }
    public double incomeAverage(){
        return Math.round(income*100.0/incomeCount)/100.0;
    }
    public double outcomeAverage(){
        return Math.round(outcome*100.0/outcomeCount)/100.0;
    }
    public ArrayList<Entry> getEntryList(){
        return projectEntries;
    }
    public Entry getEntryInPos(int index){
        if(index<0 || index>projectEntries.size()) return null;
        return projectEntries.get(index);
    }
    public Date getDate(){
        return create_date;
    }
    public void setDate(Timestamp timestamp){
        create_date = timestamp;
    }
    public ProjectType getProjectType(){
        return projectType;
    }
    public void setProjectType(ProjectType type){
        projectType=type;
    }
    public void setTypeFromString(String type){
        for (ProjectType _type:ProjectType.values()) {
            if(_type.getName().equals(type)){
                projectType=_type;
            }
        }
    }
    public void setTypeFromInt(int type){
        if(type<0 || type >= ProjectType.values().length){
            projectType=ProjectType.PERSONAL;
        }
        else projectType=ProjectType.values()[type];
    }
    public String getProjectName(){
        return projectName;
    }
    public void setProjectName(String newName)
    {
        projectName = newName;
        Projects.changeProjectName(getProjectID(), getProjectName());
    }
    public int getProjectID(){
        return projectId;
    }

    @Override
    public boolean equals(Object obj) {
        return ((Project)obj).getProjectID()==getProjectID();
    }
}
