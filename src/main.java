import Mysql.Controllers.Projects;
import Mysql.Mysql;
import Objects.Entry;
import Windows.Main;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class main {
    private static Mysql mysql;
    public static void main(String args[]){
        OnApplicationStart();
        Runtime.getRuntime().addShutdownHook(new Thread(main::OnApplicationExit));
    }
    private static void OnApplicationStart(){
        String user="";
        String password="";
        String host="";
        String db="";
        try {
            File fXmlFile = new File("config/mysql.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            user=doc.getElementsByTagName("user").item(0).getTextContent();
            password=doc.getElementsByTagName("password").item(0).getTextContent();
            host=doc.getElementsByTagName("host").item(0).getTextContent();
            db=doc.getElementsByTagName("database").item(0).getTextContent();
        }catch (Exception ignored){
        }
        mysql = new Mysql(user, password, host, db);
        mysql.connect();
        Projects.setConnectionInstance(mysql);
        Projects.loadAll();
        new Main();
    }
    private static void OnApplicationExit(){
        mysql.close();
    }
}
