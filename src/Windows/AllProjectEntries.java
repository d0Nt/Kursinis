package Windows;

import Mysql.Controllers.Projects;
import Objects.Definitions.EntryType;
import Objects.Definitions.ProjectType;
import Objects.Entry;
import Objects.Project;
import Windows.Elements.AppStyle;
import Windows.Elements.Elements.SelectList;
import Windows.Elements.Elements.Table;
import Windows.Elements.PopUps.InputPopUp;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AllProjectEntries extends AppWindow {
    private Project currentProject=null;
    private Table table=null;
    private AppWindow parent;
    public AllProjectEntries(AppWindow parent, int projectId){
        this.parent = parent;
        Projects.loadProjectEntries(projectId);
        currentProject = Projects.getProjectById(projectId);
        CreateWindow("Visi projekto įrašai", 600, 450);
        setLayout(null);
        AppStyle.AddHeader(this, null);
        addInfoPanels();
        SetVisible();
    }
    @Override
    public void resetView(){
        table.hide();
        addInfoPanels();
        setVisible(true);
    }
    private void addInfoPanels(){
        String[] columns = new String[] {
                "rec_id","list_id","Įvedimo data", "Aprašymas", "Suma", " "
        };
        BufferedImage editIcon = null;
        try {
            editIcon = ImageIO.read(new File("images/edit.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ImageIcon icon=null;
        if(editIcon!=null ) icon=new ImageIcon(editIcon.getScaledInstance(16,16,Image.SCALE_SMOOTH));
        Object[][] data = new Object[currentProject.getEntryList().size()][5];
        for (int i = 0; i < currentProject.getEntryList().size(); i++) {
            Entry entry = currentProject.getEntryList().get(i);
            data[i] = new Object[]{entry.getId(), i, getTimeString(entry.getDate()), entry.getDescription(), ((entry.getType() == EntryType.NEGATIVE) ? "-" : "+") + entry.getAmount(), (editIcon != null) ? icon : "none"};
        }
        table = new Table(this, data, columns);
        table.hideColumn(0);
        table.hideColumn(0);
        table.setBounds(10,100, 580,350);
        table.show();
        JLabel search = new JLabel("Paieška:");
        search.setBounds(10,60,100,20);
        add(search);
        JTextField text = new JTextField();
        text.setBounds(70,60,100,20);
        text.getDocument().addDocumentListener(
                new DocumentListener() {
                    public void changedUpdate(DocumentEvent e) {
                        table.filterAll(text.getText());
                    }
                    public void insertUpdate(DocumentEvent e) {
                        table.filterAll(text.getText());
                    }
                    public void removeUpdate(DocumentEvent e) {
                        table.filterAll(text.getText());
                    }
                });
        add(text);
    }
    private String getTimeString(Date date){
        if(date==null) return "0000/00/00 00:00";
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        return df.format(date);
    }
    @Override
    public void onWindowClose() {
        super.onWindowClose();
        parent.setEnabled(true);
        this.Dispose();
    }

    @Override
    public void onDoubleClickedOnTable(int rowId, int columnId){
        if(columnId==3) {
            if(!isNumeric(""+table.getValueAt(rowId, 1))) return;
            new EditEntry(this, currentProject, (int) table.getValueAt(rowId, 1));
            this.setEnabled(false);
        }
    }
    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }
}
