package Windows;

import Mysql.Controllers.Projects;
import Objects.Definitions.EntryType;
import Objects.Definitions.ProjectType;
import Objects.Entry;
import Objects.Project;
import Windows.Elements.AppStyle;
import Windows.Elements.Elements.SelectList;
import Windows.Elements.Elements.Table;
import Windows.Elements.PopUps.InputPopUp;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProjectView extends AppWindow {
    private Project currentProject=null;
    private SelectList projectSettings = null;
    private SelectList dataSettings = null;
    private JPanel project_info = null;
    private JPanel money_info = null;
    private Table table=null;
    public ProjectView(int projectId){
        Projects.loadProjectEntries(projectId);
        currentProject = Projects.getProjectById(projectId);
        CreateWindow("Pelno skaičiuoklė", 600, 450);
        setLayout(null);
        AppStyle.AddHeader(this, null);
        projectSettings = new SelectList(this, projectSettingsArray(), "project_settings");
        projectSettings.setBounds(this.getWindowSize().width-210, 60, 200, 20);
        projectSettings.show();
        dataSettings = new SelectList(this, new String[]{"Pridėti naują", "Peržiūrėti visus"}, "data_settings");
        dataSettings.setBounds(this.getWindowSize().width-210, 420, 200, 20);
        dataSettings.show();
        addInfoPanels();
        SetVisible();
    }
    @Override
    public void resetView(){
        remove(project_info);
        project_info=null;
        remove(money_info);
        money_info=null;
        table.hide();
        addInfoPanels();
        setVisible(true);
    }
    private void addInfoPanels(){
        Border paneEdge = BorderFactory.createEmptyBorder(0,10,10,10);

        project_info = new JPanel();
        project_info.setBounds(0,80,300,150);
        project_info.setBorder(paneEdge);
        project_info.setLayout(new BoxLayout(project_info, BoxLayout.Y_AXIS));
        TitledBorder titled = BorderFactory.createTitledBorder("Projekto informacija");
        addCompForBorder(titled, "<html>" +
                "Projekto tipas: "+currentProject.getProjectType().getName()+"<br>" +
                "Projekto sukūrimo data: "+getTimeString(currentProject.getDate())+"<br>" +
                "Įrašų kiekis: "+currentProject.entriesCount()+"<br>" +
                "</html>", project_info);
        add(project_info);
        money_info = new JPanel();
        money_info.setBounds(300,80,300,150);
        money_info.setBorder(paneEdge);
        money_info.setLayout(new BoxLayout(money_info, BoxLayout.Y_AXIS));

        titled = BorderFactory.createTitledBorder("Pajamos/išlaidos");
        int sum=currentProject.entriesSum();
        addCompForBorder(titled, "<html>" +
                "Skirtumas: "+sum+"<br>" +
                "Būsena: "+((sum>0)?"<font color='green'>pelningas</font>":"<font color='red'>nuostolingas</font>")+"<br>" +
                "Pajamų įrašo vidurkis: "+currentProject.incomeAverage()+"<br>"+
                "Išlaidų įrašo vidurkis: "+currentProject.outcomeAverage()+"<br>"+
                "</html>", money_info);

        add(money_info);
        JLabel lab=new JLabel("Paskutiniai 5 įrašai", JLabel.CENTER);
        lab.setBounds(0,250,600,15);
        add(lab);

        String[] columns = new String[] {
                "rec_id","list_id","Įvedimo data", "Aprašymas", "Suma", " "
        };
        BufferedImage editIcon = null;
        try {
            editIcon = ImageIO.read(new File("images/edit.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ImageIcon icon=null;
        if(editIcon!=null ) icon=new ImageIcon(editIcon.getScaledInstance(16,16,Image.SCALE_SMOOTH));
        Object[][] data = new Object[][]{
                {"", "", "", "", "", ""},
                {"", "", "", "", "", ""},
                {"", "", "", "", "", ""},
                {"", "", "", "", "", ""},
                {"", "", "", "", "", ""}
        };
        for (int i = 0; i < currentProject.getEntryList().size() && i < 5; i++) {
            Entry entry = currentProject.getEntryList().get(i);
            data[i] = new Object[]{entry.getId(), i, getTimeString(entry.getDate()), entry.getDescription(), ((entry.getType() == EntryType.NEGATIVE) ? "-" : "+") + entry.getAmount(), (editIcon != null) ? icon : "none"};
        }
        table = new Table(this, data, columns);
        table.hideColumn(0);
        table.hideColumn(0);
        table.setBounds(10,300, 580,103);
        table.show();
    }
    private void addCompForBorder(Border border, String description, Container container) {
        JPanel comp = new JPanel(new GridLayout(1, 1), false);
        JLabel label = new JLabel(description, JLabel.CENTER);
        comp.add(label);
        comp.setBorder(border);

        container.add(Box.createRigidArea(new Dimension(0, 10)));
        container.add(comp);
    }
    private void onTryChangeProjectSettings(int settingID){
        switch (settingID)
        {
            case 0: break;//project name
            case 1: // project name
                changeProjectName();
                break;
            case 2:
                onRequestChangeProjectType();
                break;
            case 3:
                onTryToDeleteProject();
                break;
            case 4://LogOut
                onWindowClose();
                break;

        }
    }
    private void onRequestChangeProjectType(){
        Object[] options = {"Keisti", "Atšaukti"};
        InputPopUp popUp = new InputPopUp(this, "Projekto tipo keitimas",options);
        popUp.setMessage("Pasirinkite projekto tipą.");
        popUp.setSystemName("change_project_type");
        popUp.setSize(350, 150);
        String[] possibilities = new String[ProjectType.values().length];
        for(int i=0;i<ProjectType.values().length;i++)
            possibilities[i]=ProjectType.values()[i].getName();
        popUp.setSelectionValues(possibilities);
        popUp.setDefaultValue(currentProject.getProjectType().getName());
        popUp.setResizable(false);
        popUp.show();

    }
    private void onTryToDeleteProject(){
        Object[] options = {"Ištrinti", "Atšaukti"};
        InputPopUp popUp = new InputPopUp(this, "Projekto ištrynimas",options);
        popUp.setMessage("Norint IŠTRINTI projektą įveskite slaptažodį ir spaukite\n \"Ištrinti\".");
        popUp.setSystemName("delete_project");
        popUp.setSize(350, 150);
        popUp.setResizable(false);
        popUp.show();
    }
    private void changeProjectName(){
        Object[] options = {"Keisti", "Atšaukti"};
        InputPopUp popUp = new InputPopUp(this, "Projekto pavadinimo keitimas",options);
        popUp.setMessage("Norėdami pakeisti pavadinimą įveskitę jį ir spauskite\n \"Keisti\".");
        popUp.setDefaultValue(currentProject.getProjectName());
        popUp.setSystemName("project_name");
        popUp.setSize(350, 150);
        popUp.setResizable(false);
        popUp.show();
    }
    private String[] projectSettingsArray(){
        return new String[]{currentProject.getProjectName(), "Keisti projekto pavadinimą", "Keisti projekto tipą", "Ištrinti projektą", "Atsijungti"};
    }
    private String getTimeString(Date date){
        if(date==null) return "0000/00/00 00:00";
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        return df.format(date);
    }
    private void onDataOperation(int value){
        switch (value){
            case 0:
                new AddEntry(this, currentProject);
                this.setEnabled(false);
                break;
            case 1:
                new AllProjectEntries(this, currentProject.getProjectID());
                this.setEnabled(false);
                break;
        }
    }
    @Override
    public void onValueSelected(String list, int value) {
        super.onValueSelected(list, value);
        if(list.equals("project_settings"))
            onTryChangeProjectSettings(value);
        if(list.equals("data_settings"))
            onDataOperation(value);
    }

    @Override
    public void onWindowClose() {
        super.onWindowClose();
        new Main();
        this.Dispose();
    }

    @Override
    public boolean onInputValue(String change_param, String inputValue, InputPopUp popUp) {
        if(change_param.equals("project_name")){
            if(!popUp.buttonPressed().equals("Keisti")) return true;
            if(inputValue.length()<4){
                JOptionPane.showMessageDialog(popUp.getDialog(),
                        "Projekto pavadinimą turi sudaryti bent 4 simboliai.",
                        "Pavadinimas per trumpas",
                        JOptionPane.ERROR_MESSAGE);
                return false;
            }
            currentProject.setProjectName(popUp.getInput());
            projectSettings.ChangeItems(projectSettingsArray());
            resetView();
            return true;
        }
        if(change_param.equals("delete_project")){
            System.out.println(popUp.buttonPressed());
            if(!popUp.buttonPressed().equals("Ištrinti")) return true;
            if(!Projects.checkPassword(currentProject.getProjectID(), popUp.getInput())){
                JOptionPane.showMessageDialog(popUp.getDialog(),
                        "Įvestas slaptažodis neteisingas!",
                        "Slaptažodis neteisingas",
                        JOptionPane.ERROR_MESSAGE);
                return false;
            }
            Projects.deleteProject(currentProject.getProjectID());
            onWindowClose();
            return true;
        }
        if(change_param.equals("change_project_type")){
            if(!popUp.buttonPressed().equals("Keisti")) return true;
            currentProject.setTypeFromString(inputValue);
            Projects.updateProjectType(currentProject.getProjectID());
            resetView();
        }
        return super.onInputValue(change_param, inputValue, popUp);
    }
    @Override
    public void onDoubleClickedOnTable(int rowId, int columnId){
        if(columnId==3) {
            if(!isNumeric(""+table.getValueAt(rowId, 1))) return;
            new EditEntry(this, currentProject, (int) table.getValueAt(rowId, 1));
            this.setEnabled(false);
        }
    }
    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }
}
