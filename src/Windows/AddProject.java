package Windows;

import Mysql.Controllers.Projects;
import Objects.Definitions.EntryType;
import Objects.Definitions.ProjectType;
import Objects.Entry;
import Objects.Project;
import Windows.Elements.AppStyle;
import Windows.Elements.Elements.InputField;
import Windows.Elements.Elements.InputPassword;
import Windows.Elements.Elements.InputSelectList;

import javax.swing.*;

import static java.lang.Integer.parseInt;

public class AddProject extends AppWindow {
    private AppWindow parent;
    private InputField name;
    private InputSelectList type;
    private InputPassword password;
    private Project project;
    public AddProject(AppWindow parent) {
        project=new Project();
        this.parent=parent;
        CreateWindow("Pagrindinis", 400, 302);
        setLayout(null);
        AppStyle.AddHeader(this, "Sukurti naują projektą");
        name = new InputField(this, "Projekto pavadinimas:", 60);
        String[] select =new String[EntryType.values().length];
        for(int i = 0; i< ProjectType.values().length; i++){
            select[i]=ProjectType.values()[i].getName();
        }
        password = new InputPassword(this, "Slaptažodis:", 110);
        type=new InputSelectList(this, "Projekto tipas:", 160, select);


        JButton save = new JButton("Sukurti");
        save.setBounds((parent.getWindowSize().width-190)/2,250,190,50);
        save.addActionListener(e -> tryToSave());
        add(save);
        //
        SetVisible();
    }
    private void tryToSave() {
        if(name.getText().length()<5){
            name.errorMessage("Pavadinimas turi būti bent 5 simbolių");
            return;
        }
        if(password.getText().length()<5){
            password.errorMessage("Slaptažodis turi būti bent 5 simbolių");
            return;
        }
        project.setNewPassword(password.getText());
        project.setProjectName(name.getText());
        Projects.addProject(project);
        onWindowClose();
    }
    @Override
    public void onWindowClose() {
        this.parent.Dispose();
        this.parent = new Main();
        this.Dispose();
    }
}
