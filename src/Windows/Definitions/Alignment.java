package Windows.Definitions;

public enum Alignment {
    LEFT,
    CENTER,
    RIGHT
}
