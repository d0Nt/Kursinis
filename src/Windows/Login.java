package Windows;

import Mysql.Controllers.Projects;
import Objects.Project;
import Windows.Elements.AppStyle;
import Windows.Elements.Elements.InputPassword;

import javax.swing.*;
import java.awt.*;

public class Login extends AppWindow {
    private Project currentProject=null;
    public Login(int projectId){
        currentProject = Projects.getProjectById(projectId);
        CreateWindow("Prisijungimas", 400, 210);
        setLayout(null);
        AppStyle.AddHeader(this, null);

        JLabel projectName = new JLabel("Projekto pavadinimas:");
        projectName.setBounds((getWindowSize().width-300)/2,60,300,25);
        projectName.setFont(new Font("Arial", Font.BOLD, 15));
        projectName.setHorizontalAlignment(SwingConstants.CENTER);
        add(projectName);

        JLabel projectTitle = new JLabel(currentProject.getProjectName());
        projectTitle.setBounds(0,80,400,25);
        projectTitle.setFont(new Font("Arial", Font.BOLD, 15));
        projectTitle.setHorizontalAlignment(SwingConstants.CENTER);
        add(projectTitle);

        InputPassword input = new InputPassword(this, "Slaptažodis:", 100);

        JButton btn=AppStyle.Button("Prisijungti", getWindowSize(), 100);
        btn.addActionListener(e -> TryLogin(input));
        add(btn);

        SetVisible();
    }
    private void TryLogin(InputPassword input){
        if(currentProject!=null && Projects.checkPassword(currentProject.getProjectID(), input.getText())){
            new ProjectView(currentProject.getProjectID());
            this.Dispose();
        }
        else{
            input.errorMessage("Slaptažodis neteisingas");
        }
    }
}
