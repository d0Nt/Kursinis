package Windows;

import Mysql.Controllers.Projects;
import Objects.Project;
import Windows.Elements.AppStyle;
import Windows.Elements.ProjectList;

import javax.swing.*;
import java.awt.*;

public class Help extends AppWindow{
    private JLabel label;
    public Help() throws HeadlessException {
        CreateWindow("Pagalba", 600, 450);
        setLayout(null);
        AppStyle.AddHeader(this, null);
        createHelp();
        SetVisible();
    }
    private void createHelp(){
        label = new JLabel(
                "<html>" +
                        "<h3>Kaip sukurti projektą?</h3>" +
                        "Projektą galima sukurti pagrindiniame lange paspaudus mygtuką \"Sukurti naują projektą\". Pasirodžiusiame lange įveskite reikalingą informaciją ir spauskite sukurti projektą."+
                        "<h3>Kaip prisijungti prie egzistuojančio projekto?</h3>" +
                        "Norint prisijungti prie egzistuojančio projekto reikia pasirinkti jį iš sąrašo ir spausti \"Atidaryti\". Tada jūsų paprašys įvesti slaptažodį, įvedę jį būsite jau savo projekto valdymo lange."+
                        "<h3>Kaip peržiūrėti visus projekto įrašus?</h3>" +
                        "Projekto valdymo lange apačioje iš galimų reikšmių langelio pasirinkite \"Peržiūrėti visus\" ir atsidarys langas su visais to projekto įrašais."+
                        "<h3>Ar galima atrinkti įrašus pagal sutapimus?</h3>" +
                        "Taip, visų įrašų lange yra paieškos laukas, kuriame galiam įvesti reikšmę ir lentelėje iškart matysite rezultatą."+
                        "<h3>Kaip redaguoti įrašą?</h3>" +
                        "Įrašą galima redaguoti 2 kartus paspaudus ant pieštuko."+
                "</html>");
        label.setBounds(10, 60, getWindowSize().width-20, getWindowSize().height-20);
        add(label);
    }
    @Override
    public void onValueSelected(String list, int value) {
        super.onValueSelected(list, value);
        Project selected=Projects.getProjectByListId(value);
        if(selected==null) return;
        new Login(selected.getProjectID());
        Dispose();
    }

    @Override
    public void onWindowClose() {
        new Main();
        this.Dispose();
    }
}
