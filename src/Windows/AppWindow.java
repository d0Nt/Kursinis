package Windows;

import Windows.Elements.PopUps.InputPopUp;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

abstract public class AppWindow extends JFrame{
    private Dimension windowSize;
    private BufferedImage logo = null;
    protected void CreateWindow(String window_name, int width, int height){
        try {
            logo = ImageIO.read(new File("images/logo.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        windowSize=new Dimension(width, height);
        setUndecorated(true);
        setTitle(window_name);
        setSize(windowSize);
        setIconImage(logo);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                onWindowClose();
            }
        });
    }
    public void SetVisible(){
        setVisible(true);
    }

    public Dimension getWindowSize() {
        return windowSize;
    }

    public BufferedImage getLogo() {
        return logo;
    }

    public void AddComponent(Component comp){
        add(comp);
    }
    public void onValueSelected(String list, int value){

    }
    public void Dispose(){
        dispose();
    }
    public void resetView(){}
    public void onWindowClose(){

    }

    public boolean onInputValue(String change_param, String inputValue, InputPopUp dialog) {
        return true;
    }

    public void onDoubleClickedOnTable(int row, int column) {
    }
}
