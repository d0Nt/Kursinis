package Windows;

import Mysql.Controllers.Projects;
import Objects.Definitions.EntryType;
import Objects.Entry;
import Objects.Project;
import Windows.Elements.AppStyle;
import Windows.Elements.Elements.InputField;
import Windows.Elements.Elements.InputSelectList;

import javax.swing.*;

import static java.lang.Integer.parseInt;

public class AddEntry extends AppWindow {
    private AppWindow parent;
    private InputField description;
    private InputSelectList type;
    private InputField amount;
    private Entry entry;
    private Project project;
    public AddEntry(AppWindow parent, Project project) {
        this.project = project;
        if(project==null) return;
        entry=new Entry(project.getProjectID());
        this.parent=parent;
        CreateWindow("Įrašo pridėjimas", 400, 302);
        setLayout(null);
        AppStyle.AddHeader(this, "Pridėti įrašą");
        description = new InputField(this, "Aprašymas:", 60);
        String[] select =new String[EntryType.values().length];
        for(int i=0;i<EntryType.values().length;i++){
            select[i]=EntryType.values()[i].getName();
        }
        type=new InputSelectList(this, "Tipas:", 110, select);
        amount=new InputField(this, "Suma:", 160);

        JButton save = new JButton("Išsaugoti");
        save.setBounds((parent.getWindowSize().width-190)/2,250,190,50);
        save.addActionListener(e -> tryToSave());
        add(save);
        //
        SetVisible();
    }
    private void tryToSave() {
        if(entry==null) return;
        if(description.getText().length()<5){
            description.errorMessage("Aprašymas turi būti bent 5 simbolių ilgio");
            return;
        }
        if(!isNumeric(amount.getText())){
            amount.errorMessage("Suma turi būti skaičius");
            return;
        }
        if(parseInt(amount.getText())<1){
            amount.errorMessage("Suma turi būti teigiamas skaičius");
            return;
        }
        entry.setAmount(parseInt(amount.getText()));
        entry.setDescription(description.getText());
        if(entry.getEntryTypeFromString(type.getText())!=null)
            entry.setType(entry.getEntryTypeFromString(type.getText()));
        Projects.addEntry(entry);
        project.clearEntries();
        Projects.loadProjectEntries(project.getProjectID());
        parent.resetView();
        onWindowClose();
    }
    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }
    @Override
    public void onWindowClose() {
        this.parent.setEnabled(true);
        this.parent.resetView();
        this.Dispose();
    }
}
