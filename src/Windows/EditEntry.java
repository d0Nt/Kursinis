package Windows;

import Mysql.Controllers.Projects;
import Objects.Definitions.EntryType;
import Objects.Entry;
import Objects.Project;
import Windows.Elements.AppStyle;
import Windows.Elements.Elements.InputField;
import Windows.Elements.Elements.InputSelectList;

import javax.swing.*;

import static java.lang.Integer.parseInt;

public class EditEntry extends AppWindow {
    private AppWindow parent;
    private InputField description;
    private InputSelectList type;
    private InputField amount;
    private Entry entry;
    private Project project;
    public EditEntry(AppWindow parent, Project project, int entryId) {
        this.project = project;
        entry=project.getEntryInPos(entryId);
        if(entry==null){
            return;
        }
        this.parent=parent;
        CreateWindow("Pagrindinis", 400, 302);
        setLayout(null);
        AppStyle.AddHeader(this, "Įrašo redagavimas");
        description = new InputField(this, "Aprašymas:", 60);
        description.setDefaultValue(entry.getDescription());
        String[] select =new String[EntryType.values().length];
        for(int i=0;i<EntryType.values().length;i++){
            select[i]=EntryType.values()[i].getName();
        }
        type=new InputSelectList(this, "Tipas:", 110, select);
        type.setDefaultValue(entry.getType().ordinal());
        amount=new InputField(this, "Suma:", 160);
        amount.setDefaultValue(entry.getAmount()+"");

        JButton save = new JButton("Išsaugoti");
        save.setBounds((parent.getWindowSize().width-190)/2,250,190,50);
        save.addActionListener(e -> tryToSave());
        add(save);
        JButton delete = new JButton("Ištrinti");
        delete.setBounds(0,250,190,50);
        delete.addActionListener(e -> tryToDelete());
        add(delete);
        //
        SetVisible();
    }
    private void tryToDelete() {
        if(entry==null) return;
        Object[] options = {"Ištrinti",
                "Atšaukti"};
        int n = JOptionPane.showOptionDialog(this,
                "Ar tikrai norite ištrinti šį įrašą?",
                "Įrašo ištrynimas",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.ERROR_MESSAGE, null, options, options[1]);
        if(n==0){
            Projects.deleteEntry(entry);
            project.removeEntry(entry);
            onWindowClose();
        }
    }
    private void tryToSave() {
        if(entry==null) return;
        if(description.getText().length()<5){
            description.errorMessage("Aprašymas turi būti bent 5 simbolių ilgio");
            return;
        }
        if(!isNumeric(amount.getText())){
            amount.errorMessage("Suma turi būti skaičius");
            return;
        }
        if(parseInt(amount.getText())<1){
            amount.errorMessage("Suma turi būti teigiamas skaičius");
            return;
        }
        entry.setAmount(parseInt(amount.getText()));
        entry.setDescription(description.getText());
        if(entry.getEntryTypeFromString(type.getText())!=null)
            entry.setType(entry.getEntryTypeFromString(type.getText()));
        Projects.saveEntry(entry);
        onWindowClose();
    }
    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }
    @Override
    public void onWindowClose() {
        this.parent.setEnabled(true);
        this.parent.resetView();
        this.Dispose();
    }
}
