package Windows.Elements;

import javax.swing.*;
import java.awt.*;

public class JListCeilRenderer extends DefaultListCellRenderer {
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        setHorizontalAlignment(SwingConstants.CENTER);
        if (isSelected) {
            c.setBackground(new Color(44, 163, 233,200));
            c.setForeground(new Color(0xffffdd));
        }
        return c;
    }
}