package Windows.Elements;

import Mysql.Controllers.Projects;
import Objects.Project;
import Windows.AppWindow;

import javax.swing.*;

public class ProjectList{
    public static JList list = null;
    public static void AddProjectList(AppWindow window) {
        DefaultListModel model = new DefaultListModel();
        list = new JList(model);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        AppStyle.SetListStyle(list);
        JScrollPane scrollPanel = new JScrollPane(list, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        AppStyle.SetCustomScrollStyle(scrollPanel);
        for (Project project: Projects.getProjectList()) {
            model.addElement(project.getProjectName());
        }
        scrollPanel.setBounds(0 , 70, window.getWindowSize().width, 200);
        scrollPanel.getViewport().setOpaque(false);
        scrollPanel.setOpaque(false);
        window.add(scrollPanel);
    }
    public static void RemoveProjectList(AppWindow window){
        window.remove(list);
        list=null;
    }
}
