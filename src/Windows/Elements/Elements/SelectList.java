package Windows.Elements.Elements;

import Windows.AppWindow;

import javax.swing.*;

public class SelectList extends Element{
    private JComboBox select;
    private JLabel title = null;
    private JLabel errorMsg=null;
    public SelectList(AppWindow parent, String[] selectFrom, String selectList) {
        super(parent);
        this.select = new JComboBox(selectFrom);
        this.select.setSelectedIndex(0);
        this.select.addActionListener(e -> parent.onValueSelected(selectList, this.select.getSelectedIndex()));
    }
    public void ChangeItems(String[] newItems){
        this.select.removeAllItems();
        for (String item:newItems) {
            this.select.addItem(item);
        }
    }
    public void setDefaultValue(int index){
        this.select.setSelectedIndex(index);
    }
    @Override
    public void show() {
        parent.add(this.select);
    }

    @Override
    public void setBounds(int x, int y, int width, int height) {
        this.select.setBounds(x, y, width, height);
    }
}
