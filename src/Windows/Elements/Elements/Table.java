package Windows.Elements.Elements;

import Windows.AppWindow;

import javax.swing.*;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;

public class Table extends Element {
    private JTable table;
    JScrollPane panel;
    TableRowSorter<TableModel> sorter;
    public Table(AppWindow _parent, Object[][] data, String[] columns)
    {
        super(_parent);
        table = new JTable(data, columns) {
            private static final long serialVersionUID = 1L;
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            @Override
            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 3: return ImageIcon.class;
                    default: return String.class;
                }
            }
        };
        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                JTable table =(JTable) mouseEvent.getSource();
                Point point = mouseEvent.getPoint();
                int row = table.rowAtPoint(point);
                int column = table.columnAtPoint(point);
                if (mouseEvent.getClickCount() == 2) {
                    parent.onDoubleClickedOnTable(row, column);
                }
            }
        });
        sorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(sorter);
        panel=new JScrollPane(table);
    }
    public void filter(String filterText, int column){
        RowFilter<TableModel, Object> rf = null;
        try {
            rf = RowFilter.regexFilter(filterText, column);

        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        sorter.setRowFilter(rf);
    }
    public void filterAll(String filterText){
        RowFilter<TableModel, Object> rf = null;
        try {
            rf = RowFilter.orFilter(Arrays.asList(RowFilter.regexFilter(filterText,2),
                    RowFilter.regexFilter(filterText, 3),
                    RowFilter.regexFilter(filterText, 4)));

        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        sorter.setRowFilter(rf);
    }
    public void hideColumn(int column){
        TableColumnModel tcm = table.getColumnModel();
        tcm.removeColumn(tcm.getColumn(column));
    }
    public void unhideColumn(int column){
        TableColumnModel tcm = table.getColumnModel();
        tcm.addColumn(tcm.getColumn(column));
    }
    public Object getValueAt(int row, int column){
        return table.getModel().getValueAt(row, column);
    }
    public void show(){
        parent.add(panel);
    }
    public void hide(){parent.remove(panel);}
    public void setBounds(int x, int y, int width, int height){
        panel.setBounds(x, y, width, height);
    }
}
