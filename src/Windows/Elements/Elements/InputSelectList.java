package Windows.Elements.Elements;

import Windows.AppWindow;

import javax.swing.*;
import java.awt.*;

public class InputSelectList extends Element{
    private JLabel title = null;
    private JLabel errorMsg=null;
    private JComboBox select;
    public InputSelectList(AppWindow parent, String title, int heightOffset, String[] selectFrom) {
        super(parent);
        this.heightOffset=heightOffset;
        this.parent=parent;
        this.title = new JLabel(title);
        this.title.setBounds((parent.getWindowSize().width-300)/2, heightOffset,300,25);
        parent.add(this.title);

        this.select = new JComboBox(selectFrom);
        this.select.setSelectedIndex(0);
        this.select.setBounds((parent.getWindowSize().width-300)/2,heightOffset+20,300,25);
        parent.add(this.select);

        this.errorMsg = new JLabel("");
        this.errorMsg.setBounds((parent.getWindowSize().width-300)/2, heightOffset+40,300,25);
        this.errorMsg.setFont(new Font("Arial", Font.PLAIN, 10));
        this.errorMsg.setForeground(Color.RED);
        parent.add(this.errorMsg);
    }
    @Override
    public String getText(){
        return (String)this.select.getSelectedItem();
    }
    public void setDefaultValue(int index){
        this.select.setSelectedIndex(index);
    }
    public void changeItems(String[] newItems){
        this.select.removeAllItems();
        for (String item:newItems) {
            this.select.addItem(item);
        }
    }
}
