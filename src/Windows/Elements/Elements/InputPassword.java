package Windows.Elements.Elements;

import Windows.AppWindow;
import Windows.Definitions.Alignment;

import javax.swing.*;
import java.awt.*;

public class InputPassword extends Element {
    private JLabel title = null;
    private JLabel errorMsg=null;
    private JTextField field = null;
    public InputPassword(AppWindow parent, String title, int heightOffset) {
        super(parent);
        this.heightOffset=heightOffset;
        this.parent=parent;
        this.title = new JLabel(title);
        this.title.setBounds((parent.getWindowSize().width-300)/2, heightOffset,300,25);
        parent.add(this.title);

        field = new JPasswordField(10);
        field.setBounds((parent.getWindowSize().width-300)/2+20,heightOffset,300,25);
        setAlignment(this.alignment);
        parent.add(field);

        this.errorMsg = new JLabel("");
        this.errorMsg.setBounds((parent.getWindowSize().width-300)/2, heightOffset+40,300,25);
        this.errorMsg.setFont(new Font("Arial", Font.PLAIN, 10));
        this.errorMsg.setForeground(Color.RED);
        parent.add(this.errorMsg);
    }
    public void errorMessage(String error){
        errorMsg.setText(error);
    }
    @Override
    public void setAlignment(Alignment alignment){
        super.setAlignment(alignment);
        if(this.alignment==Alignment.CENTER) {
            field.setBounds((parent.getWindowSize().width - field.getSize().width) / 2, this.heightOffset + 20, field.getSize().width, field.getSize().height);
            title.setBounds((parent.getWindowSize().width - field.getSize().width) / 2, this.heightOffset, field.getSize().width, field.getSize().height);
        }else if(this.alignment==Alignment.LEFT){
            title.setBounds(10, this.heightOffset+20, field.getSize().width, field.getSize().height);
            field.setBounds(10, this.heightOffset, field.getSize().width, field.getSize().height);
        }else if(this.alignment==Alignment.RIGHT){
            field.setBounds(parent.getWindowSize().width-field.getSize().width-10, this.heightOffset + 20, field.getSize().width, field.getSize().height);
            field.setBounds(parent.getWindowSize().width-field.getSize().width-10, this.heightOffset, field.getSize().width, field.getSize().height);
        }
    }
    @Override
    public String getText() {
        return field.getText();
    }
}
