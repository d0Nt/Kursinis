package Windows.Elements;

import Windows.AppWindow;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;

public class AppStyle {
    public static void AddHeader(AppWindow window, String customTitle){
        JLabel lab=new JLabel(customTitle==null?window.getTitle():customTitle, SwingConstants.CENTER);
        lab.setFont(new Font("Arial", Font.BOLD, 17));
        lab.setForeground(new Color(0xffffdd));
        lab.setOpaque(true);
        lab.setBackground(new Color(0x2f3234));
        lab.setBounds(0 , 0, window.getWindowSize().width, 50);
        window.add(lab);
        JLabel imageLabel = new JLabel(new ImageIcon(window.getLogo().getScaledInstance(46,46, Image.SCALE_SMOOTH)));
        imageLabel.setBounds(12 , 2, 46, 46);
        lab.add(imageLabel);
        //
        JButton exitWindow = new JButton("X");
        exitWindow.setBounds(window.getWindowSize().width-30,15,20,20);
        exitWindow.setOpaque(false);
        exitWindow.setContentAreaFilled(false);
        exitWindow.setFont(new Font("Arial", Font.BOLD, 17));
        exitWindow.setForeground(new Color(0xffffdd));
        exitWindow.setBorderPainted(false);
        exitWindow.setFocusPainted(false);
        exitWindow.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.CYAN, 0),
                BorderFactory.createLineBorder(Color.BLACK, 0)));
        exitWindow.addActionListener(e -> window.onWindowClose());
        lab.add(exitWindow);
    }
    public static void SetCustomScrollStyle(JScrollPane panel){
        panel.setComponentZOrder(panel.getVerticalScrollBar(), 0);
        panel.setComponentZOrder(panel.getViewport(), 1);
        panel.getVerticalScrollBar().setOpaque(false);
        panel.setLayout(new ScrollPaneLayout() {
            @Override
            public void layoutContainer(Container parent) {
                JScrollPane scrollPane = (JScrollPane)parent;

                Rectangle availR = scrollPane.getBounds();
                availR.x = availR.y = 0;

                Insets insets = parent.getInsets();
                availR.x = insets.left;
                availR.y = insets.top;
                availR.width  -= insets.left + insets.right;
                availR.height -= insets.top  + insets.bottom;

                Rectangle vsbR = new Rectangle();
                vsbR.width  = 12;
                vsbR.height = availR.height;
                vsbR.x = availR.x + availR.width - vsbR.width;
                vsbR.y = availR.y;

                if(viewport != null) {
                    viewport.setBounds(availR);
                }
                if(vsb != null) {
                    vsb.setVisible(true);
                    vsb.setBounds(vsbR);
                }
            }
        });
        panel.getVerticalScrollBar().setUI(new BasicScrollBarUI() {
            private final Dimension d = new Dimension();
            @Override protected JButton createDecreaseButton(int orientation) {
                return new JButton() {
                    @Override public Dimension getPreferredSize() {
                        return d;
                    }
                };
            }
            @Override protected JButton createIncreaseButton(int orientation) {
                return new JButton() {
                    @Override public Dimension getPreferredSize() {
                        return d;
                    }
                };
            }
            @Override
            protected void paintTrack(Graphics g, JComponent c, Rectangle r) {}
            @Override
            protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
                Graphics2D g2 = (Graphics2D)g.create();
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
                Color color = null;
                JScrollBar sb = (JScrollBar)c;
                if(!sb.isEnabled() || r.width>r.height) {
                    return;
                }else if(isDragging) {
                    color = new Color(44, 163, 233,200);
                }else if(isThumbRollover()) {
                    color = new Color(44, 163, 233,200);
                }else {
                    color = new Color(44, 163, 233,200);
                }
                g2.setPaint(color);
                g2.fillRoundRect(r.x,r.y,r.width,r.height,10,10);
                g2.setPaint(Color.WHITE);
                g2.drawRoundRect(r.x,r.y,r.width,r.height,10,10);
                g2.dispose();
            }
            @Override
            protected void setThumbBounds(int x, int y, int width, int height) {
                super.setThumbBounds(x, y, width, height);
                scrollbar.repaint();
            }
        });
    }
    public static void SetListStyle(JList list){
        list.setBackground(new Color(0,0,0,0));
        list.setFixedCellHeight(20);
        list.setOpaque(false);
        list.setCellRenderer(new JListCeilRenderer());
    }
    public static JButton Button(String text, Dimension windowSize, int buttonWidth){
        JButton but = new JButton(text);
        but.setBounds((windowSize.width-buttonWidth)/2, 165, buttonWidth, 40);
        return but;
    }

}
