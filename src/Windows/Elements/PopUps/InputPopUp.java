package Windows.Elements.PopUps;

import Windows.AppWindow;

import javax.swing.*;
import java.awt.*;

public class InputPopUp extends PopUp {

    public InputPopUp(AppWindow parent, String title, Object[] options) {
        super(parent, title, options);
        pane.setWantsInput(true);
        pane.addPropertyChangeListener(
                e -> {
                    String prop = e.getPropertyName();
                    if (dialog.isVisible()
                            && (e.getSource() == pane)
                            && (prop.equals(JOptionPane.VALUE_PROPERTY))) {
                        if(parent.onInputValue(popUpName, (String)pane.getInputValue(), this))
                            dialog.setVisible(false);
                    }
                });
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        dialog.setLocation(dim.width/2-dialog.getSize().width/3, dim.height/2-dialog.getSize().height/2);
    }
    public void setDefaultValue(String value){
        pane.setInitialSelectionValue(value);
    }
    public void setSelectionValues(String[] selectionValues){
        pane.setSelectionValues(selectionValues);
    }
    public String getInput(){
        return (String)pane.getInputValue();
    }
}
