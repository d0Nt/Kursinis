package Windows.Elements.PopUps;


import Windows.AppWindow;

import javax.swing.*;
import java.awt.*;

public class PopUp {
    protected JOptionPane pane;
    protected JDialog dialog;
    protected String popUpName="none";
    public PopUp(AppWindow parent, String title, Object[] options) {
        pane = new JOptionPane("", JOptionPane.PLAIN_MESSAGE,
                JOptionPane.OK_CANCEL_OPTION, null,
                options, null);
        dialog = new JDialog(parent, title, true);
    }
    public void setUndecorated(boolean value){
        dialog.setUndecorated(value);
    }
    public void setSize(int width, int height){
        dialog.setSize(new Dimension(width,height));
    }
    public void setResizable(boolean value){
        dialog.setResizable(value);
    }
    public void show(){
        dialog.setContentPane(pane);
        dialog.pack();
        dialog.setVisible(true);
    }
    public void setSystemName(String name){
        popUpName=name;
    }
    public void setMessage(String message){
        pane.setMessage(message);
    }

    public JDialog getDialog() {
        return dialog;
    }

    public String buttonPressed(){
        return (String)pane.getValue();
    }
}
