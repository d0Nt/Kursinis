package Windows;

import Mysql.Controllers.Projects;
import Objects.Project;
import Windows.Elements.AppStyle;
import Windows.Elements.ProjectList;

import javax.swing.*;
import java.awt.*;

public class Main extends AppWindow{
    private JButton openProject;
    private JButton help;
    private JButton createProject;
    private JLabel label;
    public Main() throws HeadlessException {
        CreateWindow("Pagrindinis", 400, 380);
        setLayout(null);
        label = new JLabel("Pasirinkite projektą iš sąrašo:");
        label.setBounds(0,50,400,20);
        label.setHorizontalAlignment(0);
        add(label);
        AppStyle.AddHeader(this, "Projekto pasirinkimas");
        ProjectList.AddProjectList(this);
        createButtons();
        SetVisible();
    }
    private void addProject(){
        new AddProject(this);
        this.setEnabled(false);
    }
    private void createButtons(){
        openProject = new JButton("Atidaryti");
        openProject.setBounds(getWindowSize().width-195,270,190,50);
        openProject.addActionListener(e -> {
            if(ProjectList.list==null) return;
            onValueSelected("project_selection", ProjectList.list.getSelectedIndex());
        });
        add(openProject);
        help = new JButton("Pagalba");
        help.setBounds(5,270,190,50);
        help.addActionListener(e -> {
            new Help();
            this.Dispose();
        });
        add(help);
        //
        createProject = new JButton("Sukurti naują projektą");
        createProject.setBounds(5,325,getWindowSize().width-10,50);
        createProject.addActionListener(e -> addProject());
        add(createProject);
    }
    @Override
    public void resetView() {
        super.resetView();
        ProjectList.RemoveProjectList(this);
        remove(help);
        remove(openProject);
        remove(createProject);
        ProjectList.AddProjectList(this);
        createButtons();
        setVisible(true);
    }

    @Override
    public void onValueSelected(String list, int value) {
        super.onValueSelected(list, value);
        Project selected=Projects.getProjectByListId(value);
        if(selected==null) return;
        new Login(selected.getProjectID());
        Dispose();
    }

    @Override
    public void onWindowClose() {
        super.onWindowClose();
        System.exit(0);
    }
}
